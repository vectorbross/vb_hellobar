(function($, window, document, Drupal) {

	function Hellobar() {
		this.id = drupalSettings.vb_hellobar.id;
		this.changed = drupalSettings.vb_hellobar.changed;
		this.height = $('.hellobar').outerHeight();
		this.init();
	}

	Hellobar.prototype.init = function() {
		this.attachHandlers();

		if(!this.getCookie('hellobar_' + this.id + '_' + this.changed)) {
			this.openHellobar()
		}
	}

	Hellobar.prototype.setCookie = function(name, value, expires) {
		var d = new Date();
		d.setTime(d.getTime() + (expires*1*60*60*1000)); // hours * minutes * seconds * miliseconds
		var cookieExpires = "expires="+ d.toUTCString();
		document.cookie = name + "=" + value + ";" + cookieExpires + ";path=/";
	}

	Hellobar.prototype.getCookie = function(cookieName) {
		var name = cookieName + "=";
		var decodedCookie = document.cookie;
		var ca = decodedCookie.split(';');
		for(var i = 0; i <ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return "";
	}

	Hellobar.prototype.attachHandlers = function() {
		var self = this;
		$('.hellobar__close').on('click', function(event) {
			event.preventDefault();
			self.closeHellobar();
		});
	}

	Hellobar.prototype.openHellobar = function() {
		$('.hellobar').addClass('hellobar--show').css('margin-top', this.height*-1);
		$('body').css({
			'margin-top': this.height,
			'transition': 'all .3s ease'
		});
	}
	Hellobar.prototype.closeHellobar = function() {
		this.setCookie('hellobar_' + this.id + '_' + this.changed, 'watched', 1);
		$('.hellobar').removeClass('hellobar--show').addClass('hellobar--hide');
		$('body').css('margin-top', '0px');
	}

	$(function() {
		var hellobar = new Hellobar;
	});

})(jQuery, window, document, Drupal);